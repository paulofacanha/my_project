#include <stdio.h>
#include "dice.h" // utilizacao da header dice.h
// esse comentario deve ir para o repositorio remoto
// comentario teste
int main() {
	initializeSeed();
	int faces;
	printf("How many faces will your dice have?\n");
	scanf("%d", &faces);
	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}
